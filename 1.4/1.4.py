import pandas as pd

# подключить titanic.csv посчитать размерность, и посмотреть типы данных в колонках
titanicData = pd.read_csv("1.4/titanic.csv")
rowsCount = titanicData.shape[0]
columnsCount = titanicData.shape[1]
dtypes = titanicData.dtypes
print(f"dimension = {rowsCount};{columnsCount}"
      f"\ndata types = \n{dtypes}")
