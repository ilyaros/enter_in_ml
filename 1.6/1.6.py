import pandas as pd

# Пересчитаем число ног у героев игры Dota2!
# Сгруппируйте героев из датасэта по числу их ног (колонка legs), и заполните их число в задании ниже.
dota = pd.read_csv("1.6/dota_hero_stats.csv")
print(dota.groupby('legs').describe())


# К нам поступили данные из бухгалтерии о заработках Лупы и Пупы за разные задачи!
# Посмотрите у кого из них больше средний заработок в различных категориях (колонка Type)
# и заполните таблицу, указывая исполнителя с большим заработком в каждой из категорий.
lupupa = pd.read_csv('1.6/accountancy.csv')
print(lupupa.groupby(['Executor', 'Type']).mean())


# Продолжим исследование героев Dota2.
# Сгруппируйте по колонкам attack_type и primary_attr и выберите самый распространённый набор характеристик.
print(dota.groupby(['attack_type', 'primary_attr']).describe())


# Аспирант Ростислав изучает метаболом водорослей и получил такую табличку.
# В ней он записал вид каждой водоросли, её род (группа, объединяющая близкие виды),
# группа (ещё одно объединение водорослей в крупные фракции) и концентрации анализируемых веществ.
#
# Помогите Ростиславу найти среднюю концентрацию каждого из веществ в каждом из родов (колонка genus)!
# Для этого проведите группировку датафрэйма, сохранённого в переменной concentrations,
# и примените метод, сохранив результат в переменной mean_concentrations.
concentrations = pd.read_csv('1.6/algae.csv')
mean_concentrations = concentrations.groupby('genus').mean()
print(f"mean_concentrations = {mean_concentrations}")


# Пользуясь предыдущими данными, укажите через пробел (без запятых) чему равны минимальная,
# средняя и максимальная концентрации аланина (alanin) среди видов рода Fucus.
# Округлите до 2-ого знака, десятичным разделителем является точка.
alanin_aggregate = concentrations.groupby('genus').aggregate({'alanin': 'describe'})
alanin_in_fucus = alanin_aggregate.loc['Fucus']
print(alanin_in_fucus.loc[[('alanin', 'min'), ('alanin', 'mean'), ('alanin', 'max')]].round(2))
