from collections import Counter

import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV

'''
Переберите параметры с помощью GridSearchCV и обучите Random Forest на данных, указанных в предыдущем стэпе. 
Передайте в GridSearchCV модель с указанием random_state

RandomForestClassifier(random_state=0)
Параметры для выбора -

n_estimators: от 10 до 50 с шагом 10
max_depth: от 1 до 12 с шагом 2
min_samples_leaf: от 1 до 7
min_samples_split: от 2 до 9 с шагом 2
Укажите cv=3. Для ускорения расчётов в GridSearchCV можно указать n_jobs=-1, чтобы использовать все процессоры.

Какие параметры Random Forest были отобраны как наилучшие для решения на этих данных?
'''
train = pd.read_csv('3.5/training_mush.csv')
train = train.rename(columns={'class': 'cl'})
x = train.drop('cl', axis=1)
y = train.cl
parametrs = {'n_estimators': range(10, 50, 10),
             'max_depth': range(1, 12, 2),
             'min_samples_leaf': range(1, 7),
             'min_samples_split': range(2, 9, 2)}
rf = RandomForestClassifier(random_state=0)
search = GridSearchCV(rf, parametrs, cv=3, n_jobs=-1)
search.fit(x, y)
best_forest = search.best_estimator_
print(best_forest.get_params())



'''
Теперь у нас есть классификатор, определяющий какие грибы съедобные, а какие нет, испробуем его! 
Предскажите съедобность этих данных грибов и напишите в ответ число несъедобных грибов (класс равен 1).

P.S. ответ = 976
'''
train = pd.read_csv('3.5/training_mush.csv')
train = train.rename(columns={'class': 'cl'})
x = train.drop('cl', axis=1)
y = train.cl
parametrs = {'n_estimators': range(10, 50, 10),
             'max_depth': range(1, 12, 2),
             'min_samples_leaf': range(1, 7),
             'min_samples_split': range(2, 9, 2)}
rf = RandomForestClassifier(random_state=0)
search = GridSearchCV(rf, parametrs, cv=3, n_jobs=-1)
search.fit(x, y)
best_forest = search.best_estimator_
test = pd.read_csv('3.5/testing_mush.csv')
print(Counter(best_forest.predict(test)))



'''
На Землю нападают войска жукеров, и в их флоте присутствуют транспортники, истребители и крейсеры. 
Для борьбы с каждым типом кораблей используется свой вид оружия. Как аналитику из Штаба Обороны, 
вам поручено разработать модель, предсказывающую какие корабли участвуют в атаке, 
чтобы успешно отбить нападения на различные области планеты

Данных удалось собрать немного, и предсказывать придётся гораздо больший по объёму массив.

Обучите модель и предскажите классы кораблей для новых поступающих данных. 
Укажите в ответе через пробел число крейсеров, транспортников и истребителей.
'''
train = pd.read_csv('3.5/invasion.csv')
x = train.drop('class', axis=1)
y = train['class'].map({'transport': 0, 'fighter': 1, 'cruiser': 2})
parametrs = {'n_estimators': range(10, 50, 10),
             'max_depth': range(1, 12, 2),
             'min_samples_leaf': range(1, 7),
             'min_samples_split': range(2, 9, 2)}
rf = RandomForestClassifier(random_state=0)
search = GridSearchCV(rf, parametrs, cv=3, n_jobs=-1)
search.fit(x, y)
best_forest = search.best_estimator_
test = pd.read_csv('3.5/operative_information.csv')
print(Counter(best_forest.predict(test)))
