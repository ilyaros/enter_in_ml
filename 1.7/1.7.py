import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

# Представьте, что у вас есть датафрэйм df, хранящий данные о зарплате за месяц, со всего 1-ой колонкой income.
# Укажите верные способы, как отрисовать простой график зависимости зарплаты от даты
df = pd.read_csv("1.7/income.csv")
df.plot()
plt.show()
sns.lineplot(data=df)
plt.show()
df.plot(kind='line')
plt.show()
plt.plot(df.index, df.income)
plt.show()
df.income.plot()
plt.show()
df['income'].plot()
plt.show()
sns.lineplot(x=df.index, y=df.income)
plt.show()


# Вам дан датасэт с 2-мя фичами (колонками).
# Постройте график распределения точек (наблюдений) в пространстве этих 2-ух переменных
# (одна из них будет x, а другая - y) и напишите число кластеров, формируемых наблюдениями.
df = pd.read_csv("1.7/dataset_209770_6.txt", sep=" ")
df.plot.scatter(x='x', y='y')
plt.show()
# P.S. ответ = 5


# Скачайте данные, представляющие геномные расстояния между видами, и постройте тепловую карту,
# чтобы различия было видно наглядно. В ответ впишите, какая картинка соответствует скачанным данным.
df = pd.read_csv('1.7/genome_matrix.csv', index_col=0)
g = sns.heatmap(df, cmap="viridis")
g.xaxis.set_ticks_position('top')
g.xaxis.set_tick_params(rotation=90)
plt.show()


# Пришло время узнать, какая роль в dota самая распространённая.
# Скачайте датасэт с данными о героях из игры dota 2 и посмотрите на распределение их возможных ролей в игре (колонка roles).
# Постройте гистограмму, отражающую скольким героям сколько ролей приписывается (по мнению Valve, конечно)
# и напишите какое число ролей у большинства героев.
df = pd.read_csv('1.7/dota_hero_stats.csv')
df['counts'] = df.roles.str.count(',') + 1
df.counts.hist()
plt.show()
# P.S. ответ = 4


#  Теперь перейдём к цветочкам. Магистрантка Адель решила изучить какие бывают ирисы.
#  Помогите Адель узнать об ирисах больше - скачайте датасэт со значениями параметров ирисов,
#  постройте их распределения и отметьте правильные утверждения, глядя на график.
iris_df = pd.read_csv('1.7/iris.csv')
sns.distplot(iris_df['petal width'], color="blue")
sns.distplot(iris_df['petal length'], color="green")
sns.distplot(iris_df['sepal width'], color="yellow")
sns.distplot(iris_df['sepal length'], color="orange")
plt.show()


# Рассмотрим длину лепестков (petal length) подробнее и воспользуемся для этого violin плотом.
# Нарисуйте распределение длины лепестков ирисов из предыдущего датасэта
# с помощью violin плота и выберите правильный (такой же) вариант среди предложенных
sns.violinplot(y=iris_df['petal length'])
plt.show()


# Продолжаем изучение ирисов! Ещё один важный тип графиков - pairplot, отражающий зависимость пар переменных друг от друга,
# а также распределение каждой из переменных. Постройте его и посмотрите на scatter плоты для каждой из пар фичей.
# Какая из пар навскидку имеет наибольшую корреляцию?
sns.pairplot(iris_df, hue = 'species')
plt.show()
