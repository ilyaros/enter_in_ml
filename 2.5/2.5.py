import pandas as pd
from sklearn import tree
from sklearn.metrics import precision_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder

'''
 Исходно в датасэтах содержались тексты песен, но Decision Tree работает с категориальными и числовыми переменными,
 а текст это... текст.
 Поэтому его необходимо преобразовать в понятную для модели форму.
 В данном случае для каждой песни просто посчитаны длина и количество некоторых знаков пунктуации.

Обучите модель на тренировочных данных, предскажите авторов для тестовых и поместите в переменную predictions.
Затем посчитайте precision score на предсказаниях и y_test, укажите параметр average='micro',
и сохраните результат в переменную precision.
'''

songs = pd.read_csv('2.5/songs.csv')
songs = songs.drop(['song', 'year', 'lyrics'], axis=1)
le = LabelEncoder()
songs.artist = le.fit_transform(songs.artist)
songs.genre = le.fit_transform(songs.genre)
X = songs.drop('artist', axis=1)
y = songs.artist
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
clf = tree.DecisionTreeClassifier(criterion='entropy', max_depth=5)
clf.fit(X_train, y_train)
predictions = clf.predict(X_test)
precision = precision_score(y_test, predictions, average='micro')
print(f"precision = {precision}")
