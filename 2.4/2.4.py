import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from sklearn import tree
'''
Скачайте тренировочный датасэт с ирисами, обучите деревья с глубиной от 1 до 100.
Целевой переменной при обучении является переменная species.
При этом записывайте его скор (DecisionTreeClassifier.score()) на тренировочных данных,
и аккуратность предсказаний (accuracy_score) на тестовом датасэте.
Затем визуализируйте зависимость скора и аккуратности предсказаний от глубины дерева и выберите правильную визуализацию из предложенных.
'''
train = pd.read_csv('2.4/train_iris.csv')
test = pd.read_csv('2.4/test_iris.csv')
x_train = train.drop(['Unnamed: 0', 'species'], axis=1)
y_train = train.species
x_test = test.drop(['Unnamed: 0', 'species'], axis=1)
y_test = test.species
max_depth_values = range(1, 100)
scores_data = pd.DataFrame()
for max_depth in max_depth_values:
    clf = tree.DecisionTreeClassifier(criterion='entropy', max_depth=max_depth)
    clf.fit(x_train, y_train)
    train_score = clf.score(x_train, y_train)
    test_score = clf.score(x_test, y_test)
    temp_score_data = pd.DataFrame({'max_depth': [max_depth], 'train_score': [train_score], 'test_score': [test_score]})
    scores_data = scores_data.append(temp_score_data)

scores_data_long = pd.melt(scores_data, id_vars=['max_depth'], value_vars=['train_score', 'test_score'],
                           var_name='set_type', value_name='score')
sns.lineplot(x='max_depth', y='score', hue='set_type', data=scores_data_long)
plt.show()



'''
Мы собрали побольше данных о котиках и собачках, и готовы обучить нашего робота их классифицировать!
Скачайте тренировочный датасэт и  обучите на нём Decision Tree.
После этого скачайте датасэт из задания и предскажите какие наблюдения к кому относятся.
Введите число собачек в вашем датасэте.
P.S. ответ = 51
'''
train = pd.read_csv('2.4/dogs_n_cats.csv')
x_train = train.drop('Вид', axis=1)
y_train = train.Вид
clf = tree.DecisionTreeClassifier(criterion='entropy')
clf.fit(x_train, y_train)
test = pd.read_json('2.4/dataset_209691_15.txt')
pred = list(clf.predict(test))
dog_count = pred.count('собачка')
print(f"dog count = {dog_count}")
