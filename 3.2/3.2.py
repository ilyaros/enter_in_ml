import pandas as pd
from sklearn.ensemble import RandomForestClassifier

# Разминочная задачка - создайте модель RandomForestClassifier с 15 деревьями и максимальной глубиной равной 5 уровням,
# поместите её в переменную rf. Обучите модель на данных x_train и y_train,
# предскажите класс для наблюдений в x_test и поместите его в переменную predictions.
#
# P.S. результат валидируется в web форме. датасет достать не удалось. Но код работает
#
# rf = RandomForestClassifier(n_estimators = 15, max_depth=5)
# rf.fit(x_train, y_train)
# predictions = rf.predict(x_test)