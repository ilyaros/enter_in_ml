import pandas as pd

student_data = pd.read_csv("1.5/StudentsPerformance.csv")


# У какой доли студентов из датасэта в колонке lunch указано free/reduced?
student_count = student_data.shape[0]
target_student_count = student_data.loc[student_data["lunch"] == "free/reduced"].shape[0]
print(f"Target student part = {target_student_count / student_count}")


# Как различается среднее и дисперсия оценок по предметам у групп студентов со стандартным или урезанным ланчем?
print('===== free/reduced =====')
print(student_data[student_data['lunch'] == 'free/reduced'].describe())
print('==== standard =====')
print(student_data[student_data['lunch'] == 'standard'].describe())


# В переменной df сохранен датафрэйм с произвольным числом колонок и строк.
# Отберите колонки, в которых есть '-' в датафрэйме df (вот соответствующий датасэт).
# Сохраните их в переменную selected_columns
df = pd.read_csv("1.5/column_hell.csv")
selected_columns = df.filter(like='-')
print(f"Selected column names = {list(selected_columns)}")
