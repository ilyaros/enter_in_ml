import matplotlib.pyplot as plt
import pandas as pd

# А пока что вот вам хакерская задача, за каким вымышленным id скрывается Анатолий Карпов - автор курса, данные которого мы анализируем?
# Введите id Анатолия Карпова, под которым он фигурирует в данных events_data_train и submissions_data_train.

events = pd.read_csv('1.11/event_data_train.csv')
submissions = pd.read_csv('1.11/submissions_data_train.csv')
events['date'] = pd.to_datetime(events.timestamp, unit='s')
events['day'] = events.date.dt.date
submissions['date'] = pd.to_datetime(submissions.timestamp, unit='s')
top_users_id = events \
    .groupby('user_id', as_index=False) \
    .count() \
    .sort_values('action', ascending=False) \
    .head(5).user_id.values
for user_id in top_users_id:
    events[events['user_id'] == user_id] \
        .groupby('day') \
        .agg({'action': 'count'}) \
        .plot(title=user_id)
    plt.show()
# Выбираем того кто начал как можно раньше (2015г) + 2 всплеска активности - в самом начале, и через год, видимо курс обновлялся
# Ответ: 1046



