import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import precision_score
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_iris
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RandomizedSearchCV
from sklearn.metrics import confusion_matrix

# Начнём с простого - создайте Decision Tree классификатор, используя одноимённый класс
# из библиотеки sklearn и сохраните его в переменную dt.
# У дерева должны быть следующие параметры:
# максимальная глубина - 5 уровней
# минимальное число образцов в вершине для разделения - 5
dt = DecisionTreeClassifier(max_depth=5, min_samples_split = 5)


# Представьте, что при помощи дерева решений мы хотим классифицировать есть или нет у пациента
# заболевание сердца (переменная num), основываясь на двух признаках: пол (sex) и наличие/отсутсвие стенокардии (exang).
# Обучите дерево решений на этих данных, используйте entropy в качестве критерия.
# Укажите, чему будет равняться значение Information Gain для переменной,  которая будет помещена в корень дерева.

train = pd.read_csv('2.7/train_data_tree.csv')
x_train = train.drop('num', axis=1)
y_train = train.num
clf = DecisionTreeClassifier(criterion='entropy')
clf.fit(x_train, y_train)
tree.plot_tree(clf, filled=True)
plt.show()
l_node = clf.tree_.children_left[0]
r_node = clf.tree_.children_right[0]
n0 = clf.tree_.n_node_samples[l_node]
e0 = clf.tree_.impurity[l_node]
n1 = clf.tree_.n_node_samples[r_node]
e1 = clf.tree_.impurity[r_node]
n = n0 + n1
ig = round(0.996 - (n0 * e0 + n1 * e1) / n, 3)
print(f"Information gain = {ig}")


# так, вам даны 2 numpy эррея с измеренными признаками ирисов и их принадлежностью к виду.
# Сначала попробуем примитивный способ с разбиением данных на 2 датасэта.
# Используйте функцию train_test_split для разделения имеющихся данных на тренировочный и
# тестовый наборы данных, 75% и 25% соответственно.
# Затем создайте дерево dt с параметрами по умолчанию и обучите его на тренировочных данных,
# а после предскажите классы, к которым принадлежат данные из тестовой выборки, сохраните результат
# предсказаний в переменную predicted.
#
# P.S. результат валидируется в web форме. датасет достать не удалось. Но код работает

# iris = load_iris()
# X = iris.data
# y = iris.target
# X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=0.25, train_size=0.75)
# dt = DecisionTreeClassifier()
# dt.fit(X_train, y_train)
# predicted = dt.predict(X_test)


# Теперь задание - осуществите перебор всех деревьев на данных ириса по следующим параметрам:
# максимальная глубина - от 1 до 10 уровней
# минимальное число проб для разделения - от 2 до 10
# минимальное число проб в листе - от 1 до 10
# и сохраните в переменную best_tree лучшее дерево. Переменную с GridSearchCV назовите search
#
# P.S. результат валидируется в web форме. датасет достать не удалось. Но код работает
#
# iris = load_iris()
# X = iris.data
# y = iris.target
# dt = DecisionTreeClassifier()
# parametrs = {'max_depth' : range(1,10), 'min_samples_split' : range(2,10), 'min_samples_leaf' : range(1,10)}
# search = GridSearchCV(dt, parametrs, cv=5)
# search.fit(X, y)
# best_tree = search.best_estimator_


# Осуществим поиск по тем же параметрам что и в предыдущем задании с помощью RandomizedSearchCV
#
# максимальная глубина - от 1 до 10 уровней
# минимальное число проб для разделения - от 2 до 10
# минимальное число проб в листе - от 1 до 10
# Cохраните в переменную best_tree лучшее дерево. Переменную с RandomizedSearchCV назовите search
#
# P.S. результат валидируется в web форме. датасет достать не удалось. Но код работает
#
# iris = load_iris()
# X = iris.data
# y = iris.target
# dt = DecisionTreeClassifier()
# parametrs = {'max_depth' : range(1,10), 'min_samples_split' : range(2,10), 'min_samples_leaf' : range(1,10)}
# search = RandomizedSearchCV(dt, parametrs)
# search.fit(X, y)
# best_tree = search.best_estimator_


# Даны 2 датасэта, к которым вы можете обращаться:
#     train - размеченный с известными правильным ответами (хранятся в колонке y)
#     test - набор, где нужно предсказать их
#
# Найдите дерево с наиболее подходящими параметрами с помощью GridSearchCV и предскажите с его помощью ответы ко 2-ому сэту!
#
# Границы параметров как раньше:
# максимальная глубина - от 1 до 10 уровней
# минимальное число проб для разделения - от 2 до 10
# минимальное число проб в листе - от 1 до 10
# Названия переменных тоже:лучшее дерево - best_tree, GridSearchCV - search, а предсказания - predictions
#
# P.S. результат валидируется в web форме. датасет достать не удалось. Но код работает
#
# x_train = train.drop('y', axis= 1)
# y_train = train.y
# dt = DecisionTreeClassifier()
# parametrs = {'max_depth' : range(1,10), 'min_samples_split' : range(2,10), 'min_samples_leaf' : range(1,10)}
# search = GridSearchCV(dt, parametrs, cv=5)
# search.fit(x_train, y_train)
# best_tree = search.best_estimator_
# predictions = best_tree.predict(test)


# Вам даны 2 эррея с истинными классами наблюдений и предсказанными - y и predictions.
# Получите по ним confusion matrix и поместите её в переменную conf_matrix.
#
# P.S. результат валидируется в web форме. датасет достать не удалось. Но код работает

# conf_matrix = confusion_matrix(y, predictions)


# Давайте найдем такой стэп, используя данные о сабмитах. Для каждого пользователя найдите такой шаг, который он не смог решить,
# и после этого не пытался решать другие шаги. Затем найдите id шага,
# который стал финальной точкой практического обучения на курсе для максимального числа пользователей.
# То есть мы исследуем следующий сценарий: человек решает стэп, не может получить правильный ответ
# и больше не возвращается к практическим задачам. Что это за шаг такой, который отпугнул максимальное число пользователей?
# P.S. ответ = 31978
submissions = pd.read_csv('2.7/submissions_data_train.csv')
only_wrong = submissions[submissions['submission_status'] == 'wrong']
print(f"hardest step ever = \n{only_wrong.step_id.value_counts().head(1)}")
